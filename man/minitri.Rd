% fields, Tools for spatial data
% Copyright 2015, Institute for Mathematics Applied Geosciences
% University Corporation for Atmospheric Research
% Licensed under the GPL -- www.gpl.org/licenses/gpl.html

\name{minitri}
\alias{minitri}
\title{
  Mini triathlon results
}
\description{
Results from a mini triathlon sponsored by Bud Lite, held in
Cary, NC, June 1990. Times are in minutes for the male 30-34 
group. Man was it hot and humid! (DN)

The events in  order were
swim:  (1/2 mile)
bike:  (15 miles)
run:   (4 miles)

<s-section 
name= "DATA DESCRIPTION">
This is a dataframe. Row names are the place within this age group based 
on total time.  
}
\arguments{
\item{swim}{
swim times 
}
\item{bike}{
bike times 
}
\item{run}{
run times 
}
}
\keyword{datasets}
% docclass is data
% Converted by Sd2Rd version 1.21.
